# DECO3801/7381
We are team named 'We are AWESOME!!!'

Team members: Thomas Chen, Bosheng Zhang, Junyi Xie, Eden Ke, Jonathan Lau, Lester Phua

Problem Statement:
Inner-city congestion remains a growing problem in today’s technological society and we need a cost-effective, ethical, and social solution.

The application will provide users with an incentive to use public transport and allow them to collaborate socially with other commuters and add friends1. It will also inform users of the impact they are having on the environment2 and allow them to redeem gift cards and free ride vouchers with points they earned by using public transport3.


# flutter_app_zoomer

A new Flutter application zoomer.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
