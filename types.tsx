export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Loading: undefined;
  Login: undefined;
  Signup: undefined;
  Home: undefined;
  Profile: undefined;
  Reward: undefined;
  Statistic: undefined;
};

export type LoadingParamList = {
  LoadingScreen: undefined;
};

export type LoginParamList = {
  LoginScreen: undefined;
};

export type SignupParamList = {
  SignupScreen: undefined;
};

export type HomeParamList = {
  HomeScreen: undefined;
};

export type ProfileParamList = {
  ProfileScreen: undefined;
};

export type RewardParamList = {
  RewardScreen: undefined;
};

export type StatisticParamList = {
  StatisticScreen: undefined;
};